Todo:
Can I code it to generate the entire table?
Can I add splitting?
Can I get it to play out a series of games and determine expected profit/loss?
Can it test additional strategies, like letting it ride? Doubling strategy?
Can it test different minimums (5, 10, 15, 25) if it matters how much I start with?