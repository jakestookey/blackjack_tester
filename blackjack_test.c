#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Begin configurable options
#define ACTION 2
#define DEBUG 0
#define NHANDS 100000
// End configurable options

#define PLAY 1
#define STAND 2
#define DOUBLE 3
#define SPLIT 4 // Not implemented

#define BET_AMOUNT 1

#define MAX_HAND 22

int action = ACTION;

#define X 140
#define Y 3

typedef int card;

card hard_hands[X][Y] = {
  {2, 2, 2},
  {2, 2, 3},
  {2, 2, 4},
  {2, 2, 5},
  {2, 2, 6},
  {2, 2, 7},
  {2, 2, 8},
  {2, 2, 9},
  {2, 2, 10},
  {2, 2, 1},
  {2, 3, 2},
  {2, 3, 3},
  {2, 3, 4},
  {2, 3, 5},
  {2, 3, 6},
  {2, 3, 7},
  {2, 3, 8},
  {2, 3, 9},
  {2, 3, 10},
  {2, 3, 1},
  {2, 4, 2},
  {2, 4, 3},
  {2, 4, 4},
  {2, 4, 5},
  {2, 4, 6},
  {2, 4, 7},
  {2, 4, 8},
  {2, 4, 9},
  {2, 4, 10},
  {2, 4, 1},
  {2, 5, 2},
  {2, 5, 3},
  {2, 5, 4},
  {2, 5, 5},
  {2, 5, 6},
  {2, 5, 7},
  {2, 5, 8},
  {2, 5, 9},
  {2, 5, 10},
  {2, 5, 11},
  {2, 6, 2},
  {2, 6, 3},
  {2, 6, 4},
  {2, 6, 5},
  {2, 6, 6},
  {2, 6, 7},
  {2, 6, 8},
  {2, 6, 9},
  {2, 6, 10},
  {2, 6, 1},
  {2, 7, 2},
  {2, 7, 3},
  {2, 7, 4},
  {2, 7, 5},
  {2, 7, 6},
  {2, 7, 7},
  {2, 7, 8},
  {2, 7, 9},
  {2, 7, 10},
  {2, 7, 1},
  {2, 8, 2},
  {2, 8, 3},
  {2, 8, 4},
  {2, 8, 5},
  {2, 8, 6},
  {2, 8, 7},
  {2, 8, 8},
  {2, 8, 9},
  {2, 8, 10},
  {2, 8, 1},
  {2, 9, 2},
  {2, 9, 3},
  {2, 9, 4},
  {2, 9, 5},
  {2, 9, 6},
  {2, 9, 7},
  {2, 9, 8},
  {2, 9, 9},
  {2, 9, 10},
  {2, 9, 1},
  {2, 10, 2},
  {2, 10, 3},
  {2, 10, 4},
  {2, 10, 5},
  {2, 10, 6},
  {2, 10, 7},
  {2, 10, 8},
  {2, 10, 9},
  {2, 10, 10},
  {2, 10, 1},
  {10, 3, 2},
  {10, 3, 3},
  {10, 3, 4},
  {10, 3, 5},
  {10, 3, 6},
  {10, 3, 7},
  {10, 3, 8},
  {10, 3, 9},
  {10, 3, 10},
  {10, 3, 1},
  {10, 4, 2},
  {10, 4, 3},
  {10, 4, 4},
  {10, 4, 5},
  {10, 4, 6},
  {10, 4, 7},
  {10, 4, 8},
  {10, 4, 9},
  {10, 4, 10},
  {10, 4, 1},
  {10, 5, 2},
  {10, 5, 3},
  {10, 5, 4},
  {10, 5, 5},
  {10, 5, 6},
  {10, 5, 7},
  {10, 5, 8},
  {10, 5, 9},
  {10, 5, 10},
  {10, 5, 1},
  {10, 6, 2},
  {10, 6, 3},
  {10, 6, 4},
  {10, 6, 5},
  {10, 6, 6},
  {10, 6, 7},
  {10, 6, 8},
  {10, 6, 9},
  {10, 6, 10},
  {10, 6, 1},
  {10, 7, 2},
  {10, 7, 3},
  {10, 7, 4},
  {10, 7, 5},
  {10, 7, 6},
  {10, 7, 7},
  {10, 7, 8},
  {10, 7, 9},
  {10, 7, 10},
  {10, 7, 1},
};

int PLAYER_CARD_1;
int PLAYER_CARD_2;
int DEALER_SHOWING;

typedef struct hand {
  card hand[MAX_HAND];
  int ncards;
  int bet;
  long int cash;
} hand;

hand playerhand;
hand dealerhand;

int hand_add_card(hand *h, card c) {
  h->hand[h->ncards] = c;
  h->ncards++;
}

init() {
  playerhand.bet = BET_AMOUNT;
  playerhand.ncards = 0;
  dealerhand.ncards = 0;
}

int setup_hands() {
  hand_add_card(&playerhand, PLAYER_CARD_1);
  hand_add_card(&playerhand, PLAYER_CARD_2);

  hand_add_card(&dealerhand, DEALER_SHOWING);
  deal_card(&dealerhand);
}

int get_card_val(card c) {
  if (c == 1) {
    return 1;
  }
  else if ((c > 1) && (c < 10)) {
    return c;
  }
  else if ((c > 9) && (c < 15)) {
    return 10;
  }
  else {
    printf("ERROR! Invalid card val\n");
    exit(1);
  }
}

int get_handscore(hand h) {
  int s = 0;
  int i;

  int has_ace = 0;
  //printf("g_h, ncards: %i\n", h.ncards);
  for (i = 0; i < h.ncards; i++) {
    if (h.hand[i] == 1) {
      has_ace = 1;
    }
    s += get_card_val(h.hand[i]);
    //printf("a - i:%i, h.hand[i]: %i, score: %i\n", i, h.hand[i], s);
  }

  // In what situation is an ace valued at 11?
  if (has_ace && s < 12) {
    s += 10;
    //printf("b%i\n", s);
  }

  return s;
}

int is_hand_soft(hand h) {
  int s = 0;
  int i;

  int has_ace = 0;

  for (i = 0; i < h.ncards; i++) {
    if (h.hand[i] == 1) {
      has_ace = 1;
    }
    s += get_card_val(h.hand[i]);
  }
 
  if (has_ace && s < 12) {
    return(1);
  }
  return(0);
}
 
int print_hands(int fhide_dealer_card) {
  int i;

  printf("%i", get_handscore(playerhand));
  if(is_hand_soft(playerhand)) printf("s");
  printf(" ");
  if (fhide_dealer_card) {
    printf("%i", get_card_val(dealerhand.hand[0]));
  } else {
    printf("%i", get_handscore(dealerhand));
  }

  printf(" ");
  printf("bet:%i $%li", playerhand.bet, playerhand.cash);
  printf("\n");
}

int deal_card(hand *h) {
  hand_add_card(h, (rand() % 13) + 1);
}

int player_double_down() {
  deal_card(&playerhand);
  if (DEBUG) printf("Player doubled down %i\n", get_card_val(playerhand.hand[playerhand.ncards-1]));

  playerhand.bet *= 2;
}

int dealer_play() {
  // while score < 17
  while(get_handscore(dealerhand) < 17) {
    deal_card(&dealerhand);
    if (DEBUG) printf("Dealer took a hit %i\n", get_card_val(dealerhand.hand[dealerhand.ncards-1]));
  }
}

int player_stand() {
}

int player_split() {
  printf("Splitting not yet implemented\n");
}

int hit_or_stand(hand phand, hand dhand) {
  // 0 stand
  // 1 hit

  // while score < 17

  if (!is_hand_soft(phand)) {
    // Strat for dealer showing 7..ace
    if ( ((get_card_val(dhand.hand[0]) > 6) && (get_card_val(dhand.hand[0]) < 11)) || get_card_val(dhand.hand[0]) == 1 ) {
      if(get_handscore(phand) < 17) {
	return 1;
      }
    }
    
    // Strat for dealer showing 4-6
    if ( (get_card_val(dhand.hand[0]) > 3) && (get_card_val(dhand.hand[0]) < 7)  ) {
      if(get_handscore(phand) < 12) {
	return 1;
      }
    }
    
    // Strat for dealer showing 2-3
    if ( (get_card_val(dhand.hand[0]) > 1) && (get_card_val(dhand.hand[0]) < 4)  ) {
      if(get_handscore(phand) < 13) {
	return 1;
      }
    }
  }
  else {
    // Soft strat for dealer showing 9,10,ace
    if ( ((get_card_val(dhand.hand[0]) > 8) && (get_card_val(dhand.hand[0]) < 11)) || get_card_val(dhand.hand[0]) == 1 ) {
      if(get_handscore(phand) < 19) {
	return 1;
      }
    }
    // Soft strat for dealer showing 2-8
    if ( (get_card_val(dhand.hand[0]) > 1) && (get_card_val(dhand.hand[0]) < 9)  ) {
      if(get_handscore(phand) < 18) {
	return 1;
      }
    }
  }
  return 0;
}

int player_play() {
  if (hit_or_stand(playerhand, dealerhand)) {
    deal_card(&playerhand);
    player_play();
  }
}

int adjust_cash(hand *h) {
  // blackjack?
  int p;
  int d;

  p = get_handscore(playerhand);
  d = get_handscore(dealerhand);

  if (p == d) {
    return;
  }
  else if (p > 21) {
    // p loses
    h->cash -= h->bet;    
  }
  else if ((d > 21) || (p > d)) {
    // p wins
    h->cash += h->bet;
  } else if (p < d) {
    // p loses
    h->cash -= h->bet;
  }
  else {
    printf("Error adjust_cash\n");
    exit(3);
  }  
}

main() {
  int i;
  int j;

  int a;

  int dc; //dealercard
  int play_cash;
  int double_cash;

  int fstand; //did player stand for this hand?

  srand(time(NULL));

  printf("   2 3 4 5 6 7 8 9 T A\n");

  for (a = 0; a < X; a++) {
      PLAYER_CARD_1 = hard_hands[a][0];
      PLAYER_CARD_2 = hard_hands[a][1];
      DEALER_SHOWING = hard_hands[a][2];

      if (a % 10 == 0) {
	printf("%2i ", get_card_val(PLAYER_CARD_1) + get_card_val(PLAYER_CARD_2));
      }

      //printf("Player is holding %i and %i, dealer showing %i, betting $%i per hand\n", PLAYER_CARD_1, PLAYER_CARD_2, DEALER_SHOWING, BET_AMOUNT);
      //printf("Total cash after playing %i hands with each style:\n", NHANDS);

      for (j = 1; j < 4; j++) { // for each action
	
	action = j;
	playerhand.cash = 0;
	
	switch (action) {
	case PLAY:
	  //printf("Don't double down: ");
	  break;
	case STAND:
	  //printf("Stand: ");
	  break;
	case DOUBLE:
	  //printf("Double: ");
	  break;
	case SPLIT:
	  //printf("Split: ");
	  break;
	}	
	
	// This differs from reality in that cards are selected at random as opposed to
	//   being drawn from a deck, so this acts like we are using infinite decks.
	for (i = 0; i < NHANDS; i++) {
	  init();
	  // For the purpose of this test, we are not considering cases where dealer has blackjack
	  //  b/c in this case, the hand ends immediately, so there is no choice to double or hit
	  while ((get_handscore(dealerhand) == 21) || (get_handscore(dealerhand) == 0)) {
	    init();
	    setup_hands();
	  }
	  
	  if (DEBUG) print_hands(1);
	  
	  if (i == 0) {
	    fstand = !(hit_or_stand(playerhand, dealerhand));
	  }

	  switch(action) {
	  case STAND:
	    player_stand();
	    break;
	  case DOUBLE:
	    player_double_down();
	    break;
	  case SPLIT:
	    player_split();
	    break;
	  case PLAY:
	    player_play();
	    
	    break;
	  default:
	    printf("ERROR, invalid ACTION %i\n", action);
	    exit(5);
	    break;
	  }
	  if (DEBUG) print_hands(0);
	  
	  dealer_play();
	  
	  adjust_cash(&playerhand);    
	  
	  if (DEBUG) print_hands(0);
	  //if (DEBUG) printf("\n");
	}
	if (action == PLAY) {
	  play_cash = playerhand.cash;
	}
	if (action == DOUBLE) {
	  double_cash = playerhand.cash;
	}
	//printf("player cash: %li\n", playerhand.cash);
      }
      //printf("%i %i\n", play_cash, double_cash);
      if (play_cash > double_cash) {
	if (fstand) {
	  printf("s ");
	} else {
	  printf("h ");
	}
	//printf("(p %i %i %i)", PLAYER_CARD_1, PLAYER_CARD_2, DEALER_SHOWING);
      }
      else {
	printf("D ");
	//printf("(D %i %i %i)", PLAYER_CARD_1, PLAYER_CARD_2, DEALER_SHOWING);
      }
      if (a % 10 == 9) {
	printf("\n");
      }
  }
}
